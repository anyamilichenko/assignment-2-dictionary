global exit
global string_copy
global string_length
global parse_int
global print_string
global parse_uint
global print_newline
global read_word
global print_char
global read_char
global print_int
global string_equals
global print_uint
global print_error

section .text 

; Принимает код возврата и завершает текущий процесс
exit:
  mov rax, 60
  syscall 


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
  xor rax, rax
  .counter:
    inc rax
    cmp byte[rdi+rax-1], 0
    jne .counter
  dec rax
  ret


; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
  push rdi
  call string_length
  pop rsi ; pointer
  mov  rdx, rax ; length
  mov  rax, 1
  mov  rdi, 1
  syscall
  ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
  mov rdi, 0xA


; Принимает код символа и выводит его в stdout
print_char:
  push rdi
  mov  rsi, rsp 
  mov  rdx, 1
  mov  rax, 1
  mov  rdi, 1
  syscall
  pop rdi
  ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    js .neg
    call print_uint
    ret
    .neg:
        push rdi
        mov rdi, "-"
        call print_char
        pop rdi
        neg rdi
        call print_uint
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.

print_uint:
    xor rdx, rdx
    push rdx    
    xor r10, r10
    mov r8, 10    
    mov rax, rdi
    .division:
        dec rsp
        inc r10
        div r8
        add rdx, "0"
        mov [rsp], dl
        xor rdx, rdx
        cmp rax, 0
        jnz .division
    mov rsi, rsp    
    mov rdi, 1      
    mov rdx, r10     
    mov rax, 1      
    syscall
    add rsp, r10    
    add rsp, 8
    ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push r12
    push r13
    xor rax, rax ; по умолч вернет 0
    xor rcx, rcx
    .loop:
        mov r13b, byte[rdi+rcx] 
        mov r12b, byte[rsi+rcx] 
        inc rcx 
        cmp r13, r12 
        je .check_null 
        pop r13
        pop r12
        ret 
    .check_null:
        test r13, r12 
        jnz .loop
        inc rax
        pop r13
        pop r12
        ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    mov rdx, 1
    push 0
    mov rsi, rsp
    syscall
    pop rax
    ret



; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
        push    r14
        push    r15
        xor    r14, r14
        mov    r15, rsi
       
        dec    r15

        cmp    rsi, 1
        jle    .exception

        .white:
            push    rdi
            call    read_char
            pop    rdi           
            cmp    al, ' '
            je    .white
            cmp    al, 10
            je    .white
            cmp    al, 13
            je    .white
            cmp    al, 9
            je    .white
            test    al, al
            jz    .success

        .chars:
            mov    byte[ rdi + r14 ], al
            inc     r14
            push    rdi
            call    read_char
            pop    rdi   
            cmp    al, ' '
            je    .success
            cmp    al, 10
            je    .success
            cmp    al, 13
            je    .success
            cmp    al, 9
            je    .success
            test    al, al
            jz    .success
            cmp    r14, r15
            je    .exception

            jmp    .chars

        .exception:
            xor    rax, rax
            jmp    .end
           
        .success:
            mov    byte[ rdi + r14 ], 0
            mov    rax, rdi
            mov    rdx, r14
           
        .end:
            pop    r15
            pop     r14
            ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось

parse_uint:
    push rcx
    push r10

    xor rcx, rcx ; 0 -> rcx (length)
    xor rax, rax ; 0 -> rax (result)
    xor rdx, rdx
    mov r10, 10

parse_uint_next:
    cmp byte[rdi+rcx], '0'
    jl parse_uint_ret
    cmp byte[rdi+rcx], '9'
    jg parse_uint_ret
    mul r10
    mov dl,  byte[rdi+rcx]
    sub dl, '0'
    add rax, rdx
    inc rcx
    jmp parse_uint_next

parse_uint_ret:
    mov rdx, rcx
    pop rcx
    pop r10
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    je parse_int_neg
    call parse_uint
    ret

parse_int_neg:
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0

string_copy:
    push rbx
    xor rax, rax
    .loop:
    	cmp rax, rdx 
    	je .notfit
    	mov bl, [rdi + rax]
    	mov byte[rsi + rax], bl
    	cmp byte[rdi + rax], 0
    	je .fit
    	inc rax
    	jmp .loop
    .notfit:
    	xor rax, rax
    .fit:
    	pop rbx
    	ret


print_error:
 	push rdi
 	call string_length
 	pop rsi ; pointer
 	mov rdx, rax ; length
 	mov rax, 1 ; write sys
 	mov rdi, 2 ; stderr
 	syscall
 	ret
