import subprocess

inp = ["", "first_discipline", "second_discipline", "abhdfs", "dddfddddddddddddddddddddddddddddddddddddddddddddddddddfdddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd"]
exp_out = ["", "programming", "databases",  "", ""]
exp_error = ["this key is not found", "", "", "this key is not found", "this key is not found"]

for i in range(len(inp)):
    p = subprocess.Popen(["./main"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate(input=inp[i].encode())
    out_result = stdout.decode().strip()
    err_result = stderr.decode().strip()
    if out_result == exp_out[i] and err_result == exp_error[i]:
        print("Test ["+str(i+1)+"] +")
    else:
        print("Test "+str(i+1)+' failed {strout: "'+out_result+'", strerr: "'+err_result+'"}, expected: {strout: "'+exp_out[i]+'", strerr: "'+ exp_error[i]+'"}')
