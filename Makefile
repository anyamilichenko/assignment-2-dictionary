ASM_FLAGS=-felf64
ASM=nasm

%.o: %.asm
	$(ASM) $(ASM_FLAGS) -o $@ $<

program: dict.o main.o lib.o
	ld -o $@ $^

.PHONY: clean

clean:
	rm *.o
