%include "lib.inc"
%include "words.inc"
%include "dict.inc"
 
%define buffer_size 255
%define dSize 8


section .rodata
READING_ERROR: db "an error occurred while reading from standard input", 0xA, 0
KEY_NOT_FOUND: db "this key is not found", 0xA, 0

section .bss
buf: resb (buffer_size+1)
 
section .text
 
global _start

_start:
    mov rsi, buffer_size;
    mov rdi, buf
    call read_word
    test rax, rax
    jz .reading_error
   
    mov rsi, entry_with_long_key
    mov rdi, buf
    call find_word
    test rax, rax
    jz .key_not_found
    
    
    mov rdi, rax
    add rdi, dSize
    push rdi
    call string_length
    pop rdi
    add rdi, 1
    add rdi, rax
    call print_string
    call print_newline
    xor rdi, rdi
    jmp exit
    
    .reading_error:
        mov rdi, READING_ERROR
        jmp .end_with_error
 
    
    .key_not_found:
        mov rdi, KEY_NOT_FOUND
 
    .end_with_error:
        call print_error
        mov rdi, 1
        jmp exit
