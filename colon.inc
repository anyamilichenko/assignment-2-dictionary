%define last_label 0

%macro colon 2
    %ifid %2 ; Check if the second argument is an identifier
        %2:
            dq last_label ; Link current node and the previous label
            db %1, 0 ; Zero-terminated string as key
        %define last_label %2 ; Redefine last_label
    %else
        %error "error: Invalid identifier"
    %endif
%endmacro
