;Указатель на нуль-терминированную строку.
;Указатель на начало словаря.

;find_word пройдёт по всему словарю в поисках подходящего ключа. Если подходящее вхождение найдено, 
;вернёт адрес начала вхождения в   словарь (не значения), иначе вернёт 0.

global find_word

%include "lib.inc"

section .text

;rdi<-Указатель на нуль-терминированную строку
;rsi<-Указатель на начало словаря
find_word:
.next:
	push rdi
	push rsi
	add rsi, 8		;переместили указатель на ключ
	call string_equals	;сравниваем ключи
	pop rsi
	pop rdi
	cmp rax, 1		;Нашли такой ключ?
	je .found_key		;ДА!-> .found_key
.notfound:			;НЕТ
	mov rsi, [rsi]
   	test rsi, rsi		;Конец списка?
	jne .next		;НЕТ -> .next
	xor rax,rax		;ДА
	ret			;уходим

.found_key:
	mov rax, rsi
	ret
